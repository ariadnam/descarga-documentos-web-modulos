from cache import Cache

if __name__ == '__main__':
    print('Ejercicio 16.10')
    c = Cache()
    c.retrieve('https://www.atleticodemadrid.com/')
    c.retrieve('https://es.wikipedia.org/wiki/Plat%C3%B3n')
    c.show('https://es.wikipedia.org/wiki/Plat%C3%B3n')
    c.show_all()