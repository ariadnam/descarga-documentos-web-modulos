

import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.descargado = False

    def retrieve(self):
        if not self.descargado:
            print("Descargando url...")
            f = urllib.request.urlopen(self.url)  # Abrimos el recurso pasado como argumento(URL)
            self.content = f.read().decode('utf-8')  # Leemos el recurso y lo decodifamos
            self.descargado = True

    def content(self):
        self.retrieve()
        return self.content
    def show(self):
        print(self.content())




if __name__ == '__main__':
    r = Robot('https://gysc.urjc.es/')
    print(r.url)
    r.show()
    r.retrieve()
