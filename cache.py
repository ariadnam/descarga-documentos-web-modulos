from robot import Robot


class Cache:
    def __init__(self):
        self.cache = {}

    def retrieve(self, url):
        if url not in self.cache:
            new = Robot(url=url)
            self.cache[url] = new

    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    def show(self, url):
        print(self.content(url))

    def show_all(self):
        for u in self.cache:
            print(u)


if __name__ == '__main__':
    c = Cache()
    c.show('https://gysc.urjc.es/')
    c.retrieve('https://www.aulavirtual.urjc.es/moodle/course/view.php?id=212556')
    c.show_all()